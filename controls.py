# -*- coding: iso-8859-1 -*-

##################################################################
#
# controls.py (23/05/2013)
#
# File with definition and implementation of the controls of the
# game.
#
# Control 	-> Base class for rendering controls.
# Movable 	-> Base control for moving controls.
# Being		-> Base control for knowing if has to be rendered.
# Square 	-> Control to render squares on the screen.
# Font 		-> Control to render text on the screen.
#
##################################################################


import pygame


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
def load_image(filename, transparent = False):
	try:
		image = pygame.image.load(filename)
	except pygame.error, message:
		raise SystemExit, message
	image = image.convert()
	if transparent:
		color = image.get_at((0,0))
		image.set_colorkey(color, RLEACCEL)
	return image


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Class: Control
#
# Represents a class for rendering any control on the display.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class Control(object):

	def __init__(self, position, size, color):

		# Parameters
		self.color = color
		self.position = list(position)
		self.size = tuple(size)

		# Variables
		self.rectangle = None
		self.surface = None


	def pre_render(self, surface):
		s = pygame.Surface(self.size)
		s.fill(self.color)
		self.rectangle = surface.blit(s, self.position)


	def render_in(self, surface):
		self.pre_render(surface)
		if self.rectangle:
			self.surface = surface.subsurface(self.rectangle)


	def update(self, *args):
		pass


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Class: Movable
#
# Represents a control that can be moved over the display.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class Movable(Control):

	def __init__(self, position, size, color):
		super(Movable, self).__init__(position, size, color)

		# Variables
		self.delta = 2
		self.direction = [1, 1]


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Class: Being
#
# Represents a control that can be alive or not.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class Being(Control):

	def __init__(self, position, size, color):
		super(Being, self).__init__(position, size, color)

		# Variables
		self.is_alive = True


	def contains(self, point):
		return pygame.Rect(self.surface.get_abs_offset(), self.size).collidepoint(point)


	def render_in(self, surface):
		if self.is_alive:
			super(Being, self).render_in(surface)


	def update(self, point, *args):
		if point and self.is_alive:
			self.is_alive = not self.contains(point)


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Class: Square
#
# Represents a square in the game.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class Square(Being, Movable):

	def __init__(self, position, size, color, type = 1):
		super(Square, self).__init__(position, size, color)

		# Parameters
		self.type = type


	def pre_render(self, surface):
		image = load_image('./images/square_%s.png' % self.type)
		self.rectangle = surface.blit(image, self.position)

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Class: Font
#
# Represents text on the display.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class Font(object):

	def __init__(self, position, color, size = 20):

		# Parameters
		self.color = color
		self.position = position
		self.size = size

		# Variables
		self.font = pygame.font.Font(None, self.size)
		self.text = ''


	def render_in(self, surface):
		self.image = self.font.render(self.text, 1, self.color)
		surface.blit(self.image, self.position)