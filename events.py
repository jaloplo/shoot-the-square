# -*- coding: iso-8859-1 -*-

##################################################################
#
# events.py (16/05/2013)
#
# File that makes more accesible the events of pygame.
#
##################################################################


import pygame


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Class: MouseEvent
#
# Represent the mouse events from pygame.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class MouseEvent(object):

	def __init__(self):
		self.pos = pygame.mouse.get_pos()