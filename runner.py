#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

##################################################################
#
# runner.py (16/05/2013)
#
# Fichero de ejecución del juego "shoot the square".
#
##################################################################


import pygame
from events import MouseEvent
from screens import *

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
def main():
    pygame.init()

    clock = pygame.time.Clock()

    display = pygame.display.set_mode((800, 600))
    pygame.display.set_caption('Shoot the square')

    screen = GameScreen()

    while True:

    	clock.tick(60)
        
        pEvent = None

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return
            elif event.type == pygame.MOUSEBUTTONUP:
                pEvent = MouseEvent()
            elif event.type == END_PLAYING_GAME_EVENT:
                screen = ResumeScreen(screen.get_points(), screen.get_time_played())
            elif event.type == START_PLAYING_GAME_EVENT:
                screen = GameScreen()

        screen.render_in(display)
        screen.update(pEvent)

    	pygame.display.flip()

    pygame.quit()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

if __name__ == "__main__": main()