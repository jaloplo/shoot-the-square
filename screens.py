# -*- coding: iso-8859-1 -*-

##################################################################
#
# screens.py (22/05/2013)
#
# File with definition and implementation of the screens of the
# game.
#
# Screen 		-> Base class for all other screens.
# DataScreen 	-> Screen with other screen inside.
# BorderScreen 	-> Screen with border.
# GameScreen 	-> Screen for the play.
#
##################################################################


import pygame, random, time
from controls import *


END_PLAYING_GAME_EVENT = pygame.USEREVENT + 1
START_PLAYING_GAME_EVENT = pygame.USEREVENT + 2


COLOR_BLACK 	= pygame.Color(  0,   0,   0, 0)
COLOR_DARK_GREY = pygame.Color( 25,  25,  25, 0)
COLOR_PEARL 	= pygame.Color(235, 235, 235, 0)
COLOR_RED 		= pygame.Color(255,   0,   0, 0)
COLOR_WHITE 	= pygame.Color(255, 255, 255, 0)
COLOR_YELLOW 	= pygame.Color(255, 255,   0, 0)


SQUARE_COLORS = (COLOR_PEARL, COLOR_RED, COLOR_WHITE, COLOR_YELLOW)


SCREEN_HEIGHT = 600
SCREEN_WIDTH = 800


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Class: Screen
#
# Represents a screen that can have different controls to render 
# inside.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class Screen(object):


	def __init__(self, width, height, position = (0, 0), color = COLOR_BLACK, background = None):

		# Parameters
		self.background = background
		self.color = color
		self.height = height
		self.position = position
		self.width = width

		# Variables
		self.controls = []
		self.mover = Mover(self)
		self.size = (self.width, self.height)

		if background:
			self.background = pygame.transform.scale(background, (self.width, self.height))


	def add_control(self, control):
		if not control in self.controls:
			self.controls.extend([control])


	def render_in(self, target):
		surface = None
		if self.background:
			rectangle = target.blit(self.background, self.position)
			surface = target.subsurface(rectangle)
		else:
			surface = target.subsurface(self.position, self.size)
			surface.fill(self.color)

		for c in self.controls:

			# This two lines allows movable controls to update
			# its position
			if isinstance(c, Movable):
				self.mover.move(c)

			if surface:
				c.render_in(surface)
	

	def update(self, *args):
		for c in self.controls:
			c.update(args)


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Class: DataScreen
#
# Represents a screen with another internal screen.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class DataScreen(Screen):


	def __init__(self, width, height, position = (0, 0), margin = (100, 50), color = COLOR_WHITE, internal_color = COLOR_BLACK, background = None):
		super(DataScreen, self).__init__(width, height, position, color, background)

		# Parameters
		self.background = None
		self.margin = margin
		
		# Variables
		internal_width = width - (2 * self.margin[0])
		internal_height = height - (2 * self.margin[1])
		internal_position = self.margin

		self.internal_screen = Screen(internal_width, internal_height, internal_position, internal_color, background)
		
		# The overriding on the "add_control" method force to call the parent "add_control" method for
		# adding the internal screen.
		super(DataScreen, self).add_control(self.internal_screen)


	def add_control(self, control):
		# This overriding is necessary to add controls to the internal screen and not to this one. All
		# the rendering controls will be in the interanl surface.
		self.internal_screen.add_control(control)


	def get_controls(self):
		return self.internal_screen.controls


	def get_mover(self):
		return self.internal_screen.mover


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Class: BorderScreen
#
# Represents a screen with border.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class BorderScreen(DataScreen):


	def __init__(self, width, height, position = (0, 0), border_size = 5, color = COLOR_WHITE, internal_color = COLOR_BLACK, background = None):
		super(BorderScreen, self).__init__(width, height, position, (border_size, border_size), color, internal_color, background)


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Class: GameScreen
#
# Represents the game screen. This class has all the elements for showing
# information to the user like punctuation, squares, time, etc.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class GameScreen(BorderScreen):


	def __init__(self):
		super(GameScreen, self).__init__(SCREEN_WIDTH, SCREEN_HEIGHT, border_size = 0, color = COLOR_BLACK, internal_color = COLOR_DARK_GREY, background = load_image('./images/background.png'))

		# Variables
		self.init_time = get_current_miliseconds()
		self.separation = (100, 75)

		player_width = self.width - (2 * self.separation[0])
		player_height = self.height - (2 * self.separation[1])
		player_position = self.separation

		self.playerScreen = BorderScreen(player_width, player_height, player_position, border_size = 2)
		self.add_control(self.playerScreen)

		self.engine = Engine(self.playerScreen)

		self.font = Font((self.separation[0], self.separation[1] - 20), COLOR_WHITE)
		self.add_control(self.font)

		self.title = Font((self.separation[0] + 150, 10), COLOR_WHITE, 50)
		self.title.text = 'Shoot the Square'
		self.add_control(self.title)

		self.time = Font(((self.separation[0] + player_width - 125), self.separation[1] - 20), COLOR_WHITE)
		self.add_control(self.time)


	def get_points(self):
		return len([c for c in self.playerScreen.get_controls() if isinstance(c, Being) and not c.is_alive])


	def get_time_played(self):
		return get_current_miliseconds() - self.init_time


	def update(self, event, *args):
		self.font.text = 'Points: %s' % self.get_points()

		current_time = self.get_time_played()
		self.time.text = 'Seconds: %s.%s' % (int(current_time / 1000), int(current_time % 1000))

		self.engine.evaluate(self.playerScreen)

		mover = self.playerScreen.get_mover()
		mover.set_elapsed_time_between_movement(mover.MAX_ELAPSED_TIME_BETWEEN_MOVEMENT - (self.get_points() * 2))

		for c in self.playerScreen.get_controls():
			if isinstance(c, Being) and event:
				c.update(event.pos, args)
			else:
				c.update(args)


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Class: ResumeScreen
#
# Represents the game screen. This class has all the elements for showing
# information to the user like punctuation, squares, time, etc.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class ResumeScreen(BorderScreen):

	def __init__(self, points, seconds):
		super(ResumeScreen, self).__init__(SCREEN_WIDTH, SCREEN_HEIGHT, border_size = 2, color = COLOR_BLACK, internal_color = COLOR_DARK_GREY)

		# Parameters
		self.seconds = seconds
		self.points = points

		# Variables
		self.font_points = Font((180, 240), COLOR_WHITE, 40)
		self.add_control(self.font_points)

		self.font_time = Font((180, 280), COLOR_WHITE, 40)
		self.add_control(self.font_time)

	def update(self, event, *args):
		self.font_points.text = 'Points: %s' % self.points
		self.font_time.text = 'Time: %s.%s seconds' % (self.seconds / 1000, self.seconds % 1000)

		if event:
			pygame.event.post(pygame.event.Event(START_PLAYING_GAME_EVENT))

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


get_current_miliseconds = lambda : int(round(time.time() * 1000))


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Class: Engine
#
# Represents the engine of creation and destruction of the controls of
# the game itself.
#
# Features:
# · Creates 5 squares as maximum
# · Creates one square every 1.5 seconds
# · When there are 6 squares the game finished
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class Engine(object):


	MAXIMUN_SQUARES_ON_SCREEN = 5
	ELAPSED_TIME_BETWEEN_CREATION = 1500 # in miliseconds


	def __init__(self, screen):

		# Parameters
		self.screen = screen

		# Variables
		self.last_creation_time = get_current_miliseconds()


		# Creation of the first square of the game
		screen.add_control(self.create())
		self.last_creation_time = get_current_miliseconds()


	def create(self):

		# This algorithm determine the size of the square.
		size = random.randint(30, 40)

		# This algorithm gets the height or width point of
		# initialization of the square in the scene.
		height, width = 5, 5
		height_or_width = random.randint(0, 1)
		if height_or_width == 0:
			height = random.randint(0, (self.screen.height+5) - size)
		elif height_or_width == 1:
			width = random.randint(0, (self.screen.width+5) - size)

		# Color initialization
		color_index = random.randint(0, len(SQUARE_COLORS) -1)

		# Type initialization
		type_index = random.randint(1, 4)

		return Square((width, height), (size, size), SQUARE_COLORS[color_index], type_index)


	def evaluate(self, screen):
		controls = [c for c in screen.get_controls() if c.is_alive]

		if len(controls) < self.MAXIMUN_SQUARES_ON_SCREEN + 1:
			if (get_current_miliseconds() - self.last_creation_time) > self.ELAPSED_TIME_BETWEEN_CREATION:
				screen.add_control(self.create())
				self.last_creation_time = get_current_miliseconds()
		else:
			pygame.event.post(pygame.event.Event(END_PLAYING_GAME_EVENT))


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Class: Mover
#
# Represents a base class for moving controls.
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class Mover(object):


	MAX_ELAPSED_TIME_BETWEEN_MOVEMENT = 100 # in miliseconds


	def __init__(self, screen):

		# Parameters
		self.screen = screen

		# Variables
		self.elapsed_time_between_movement = self.MAX_ELAPSED_TIME_BETWEEN_MOVEMENT


	def move(self, control):
		current_time = get_current_miliseconds()

		if not getattr(control, 'last_moved_time', None):
			setattr(control, 'last_moved_time', current_time)

		if (current_time - control.last_moved_time) > self.elapsed_time_between_movement:

			if control.position[0] + control.size[0] >= (self.screen.width-5) or control.position[0] < 5:
				control.direction[0] = -1 * control.direction[0]

			if control.position[1] + control.size[1] >= (self.screen.height-5) or control.position[1] < 5:
				control.direction[1] = -1 * control.direction[1]

			control.position[0] = control.position[0] + (control.delta * control.direction[0])
			control.position[1] = control.position[1] + (control.delta * control.direction[1])

			setattr(control, 'last_moved_time', current_time)


	def set_elapsed_time_between_movement(self, value):
		self.elapsed_time_between_movement = value
		if self.elapsed_time_between_movement < 1:
			self.elapsed_time_between_movement = 1